from pprint import pprint

def draw_board(n):
    # board = []
    # for i in range(n):
    #     row = []
    #     for j in range(n):
    #         row.append(0)
    #     board.append(row)
    # return board
    return [[0] * n ] * n

def is_it_safe(board, row, column):
    
    # check row
    for i in range(column):
        if board[row][i] == 1:
            return False
    
    # check positive slope diagonal
    for i, j in zip(range(row, 4, 1), # change this 4 to n once everything is hooked up
                    range(column, -1, -1)):
        if board[i][j] == 1:
            return False


    # check negative slope diagonal
    for i, j in zip(range(row, -1, -1),
                    range(column, -1, -1)):
        if board[i][j] == 1:
            return False

    # if all checks pass
    return True

def place_queens(board, column=0):

    if column >= len(board):
        return True

    # Place a queen in each column
    for i in range(len(board)):
        # Check if each row is safe
        if is_it_safe(board, i, column):
            # Place queen in this position
            board[i][column] = 1

            # Recur
            if place_queens(board, column + 1) == True:
                return True

            # If queen can't be placed, remove queen
            board[i][column] = 0
    
    # If queen can't be place in any row in this column, return False
    return False 

def print_solution(board):
    for i in range(4): # change this 4 to n once everything is hooked up
        for j in range(4):
            print (board[i][j], end= ' ')
        print()

def find_positions(n):
    board = draw_board(n)

    if place_queens(board) == False:
        return None

    print_solution(board)
    return True

print(find_positions(4))




# board = draw_board(4)
# pprint(board, width=20)

# test_board = [
#     [0, 0, 0, 0],
#     [0, 0, 0, 0],
#     [0, 0, 0, 0],
#     [0, 0, 0, 0],
# ]

# print(is_it_safe(test_board, 3, 3))

# if find_positions(4):
#     pprint(find_positions(4))
# else:
#     print("Sorry, no solutions found")

