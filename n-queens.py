from pprint import pprint

def is_safe(board, row, column, size):
    
    # check row
    for i in range(column):
        if board[row][i] == 1:
            return False
    
    # check positive slope diagonal
    for i, j in zip(range(row, 4, 1), # change this 4 to n once everything is hooked up
                    range(column, -1, -1)):
        if board[i][j] == 1:
            return False


    # check negative slope diagonal
    for i, j in zip(range(row, -1, -1),
                    range(column, -1, -1)):
        if board[i][j] == 1:
            return False

    # if all checks pass
    return True

def place_queen_in_column(board, column_index, size):
    if column_index >= size:
        return True
    for row_index in range(size):
        if is_safe(board, row_index, column_index, size):
            board[row_index][column_index] = 1
            if place_queen_in_column(board, column_index + 1, size):
                return True
            board[row_index][column_index] = 0
    return False

def n_queens(size):
    board = []
    for i in range(size):
        row = [0] * size
        board.append(row)

    place_queen_in_column(board, 0, size)

    return board


pprint(n_queens(6), width=20)