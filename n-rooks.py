## Goes column by column
##   For each column
#       goes row by row

def is_it_safe(board, row, column):
    for check_index in range(len(board)):
        if check_index == column:
            continue
        if board[row][check_index] == 1:
            return False
    return True


def place_rook_in_column(board, column=0):
    """
    If you want this to work, don't do the column
    thing, mk?
    """
    if column >= len(board):
        return True
    for row in range(len(board)):  # 0 -> how many rows
        board[row][column] = 1
        # if it's safe, go to the next column
        if is_it_safe(board, row, column):
            # place in next column
            is_it_good = place_rook_in_column(board, column + 1)
            return is_it_good
        board[row][column] = 0
    return False




# 0 means empty space
# 1 (but you could use "A" or "📗") means a rook is in the
#   space just don't use 0
ian_board = [
    [0, 0, 0, 1],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
]

if place_rook_in_column(ian_board):
    from pprint import pprint
    pprint(ian_board, width=15)
else:
    print("NO JOY!")

# americas_board = [
#     0, 0, 0, 0,
#     0, 0, 0, 0,
#     0, 0, 0, 0,
#     0, 0, 0, 0,
# ]
# americas_board[row * size + column]